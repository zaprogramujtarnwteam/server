const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
const PORT = process.env.PORT || 5000;
const backgroundWorker = require('./backgroundWorker');
const cors = require('cors');
require('dotenv').config();

// import routes
const dataRoutes = require('./routes/data');
const reportRoutes = require('./routes/report');
const imageRoutes = require('./routes/image');
const routeRoutes = require('./routes/route');
const questionRoutes = require('./routes/question');
const eventRoutes = require('./routes/event');

// parse incoming request body to req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

// routes
app.use('/api/data', dataRoutes);
app.use('/api/report', reportRoutes);
app.use('/api/image', imageRoutes);
app.use('/api/route', routeRoutes);
app.use('/api/question', questionRoutes);
app.use('/api/event', eventRoutes);

// connect to MongoDB
try {
    mongoose.connect(`mongodb://${process.env.DATABASE_USER}:${process.env.DATABASE_PASSWORD}@ds013559.mlab.com:13559/tarnow-pod-reka`, { useNewUrlParser: true });
    app.listen(PORT, '0.0.0.0', () => {
        console.log(`Server is running on PORT: ${PORT}`);
    });
    backgroundWorker.updateStationData();
    backgroundWorker.updateWeather();
} catch (err) {
    console.log(err);
}

mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
mongoose.set('count', true);