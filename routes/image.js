const express = require('express');
const multer = require('multer');
const upload = multer();

const imageController = require('../controllers/imageController');

const router = express.Router();

// add new image
router.post('/add', upload.single("image"), imageController.postAddImage);

// get image from AWS S3
router.get('/:fileName', imageController.getImage);

module.exports = router;