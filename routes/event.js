const express = require('express');

const eventController = require('../controllers/eventController');

const router = express.Router();

router.post('/add', eventController.postAddEvent);

router.get('/all', eventController.getAllEvents);

router.get('/:eventId', eventController.getEvent);

module.exports = router;