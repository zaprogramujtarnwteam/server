const express = require('express');

const dataController = require('../controllers/dataController');

const router = express.Router();

router.post('/add/stop', dataController.postAddBusStop);
router.post('/add/place', dataController.postAddPlace);
router.get('/busStops', dataController.getBusStops);
router.get('/bikeStations', dataController.getBikeStations);
router.get('/all', dataController.getAllData);
router.get('/weather', dataController.getWeather);
router.get('/place/:placeId', dataController.getPlaceById);

module.exports = router;