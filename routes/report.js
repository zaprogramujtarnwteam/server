const express = require('express');

const reportController = require('../controllers/reportController');

const router = express.Router();

router.post("/add", reportController.postAddReport);

module.exports = router;