const express = require('express');

const routeController = require('../controllers/routeController');

const router = express.Router();

router.post("/add", routeController.postAddRoute);

router.patch("/add/crucial/:routeId", routeController.postAddCrucialPoints);

router.patch("/rate/:routeId", routeController.patchRateRoute);

router.get("/", routeController.getRoutes);

module.exports = router;