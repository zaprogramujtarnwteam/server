const express = require('express');

const questionController = require('../controllers/questionController');

const router = express.Router();

// add new image
router.post('/add', questionController.postAddQuestion);

// get image from AWS S3
router.get('/', questionController.getRandomQuestions);

module.exports = router;