const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const placeSchema = new Schema({
    authorized: {
        type: Boolean,
        default: true
    },
    longitude: Number,
    latitude: Number,
    category: String,
    subcategory: String,
    name: String,
    description: String,
    rating: Array,
    workingHours: Array,
    imagePath: String,
    address: String,
    website: String,
    email: String,
    phone: String,
    priceLevel: Number,
    qrId: String
});

const Place = mongoose.model('Place', placeSchema);

exports.Place = Place;