const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const busStopSchema = new Schema({
    authorized: {
        type: Boolean,
        default: true
    },
    fullStopName: String,
    poleID: Number,
    poleDescription: String,
    stopId: Number,
    poleNumber: Number,
    zone: Number,
    street: String,
    town: String,
    municipality: String,
    longitude: Number,
    latitude: Number
});

const BusStop = mongoose.model('BusStop', busStopSchema);

exports.BusStop = BusStop;