const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const reportSchema = new Schema({
    description: String,
    date: Date,
    latitude: Number,
    longitude: Number,
    imagePath: String,
    email: String
});

const Report = mongoose.model("Report", reportSchema);

exports.Report = Report;