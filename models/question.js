const mongoose = require('mongoose');
const random = require('mongoose-simple-random');

const Schema = mongoose.Schema;

const questionSchema = new Schema({
    question: String,
    answers: [{
        text: String,
        correct: {
            type: Boolean,
            default: false
        }
    }],
    imagePath: String
});

questionSchema.plugin(random);

const Question = mongoose.model("Question", questionSchema);

exports.Question = Question;