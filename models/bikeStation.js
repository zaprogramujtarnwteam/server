const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const bikeStationSchema = new Schema({
    longitude: Number,
    latitude: Number,
    name: String,
    availableBikes: Number,
    availableStands: Number,
    standsAmount: Number
});

const BikeStation = mongoose.model('BikeStation', bikeStationSchema);

exports.BikeStation = BikeStation;