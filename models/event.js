const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const eventSchema = new Schema({
    authorized: {
        type: Boolean,
        default: true
    },
    address: String,
    name: String,
    description: String,
    startDate: Date,
    endDate: Date,
    website: String,
    tickets: String,
    imagePath: String,
    price: Number,
    organizer: String,
    category: String
});

const Event = mongoose.model('Event', eventSchema);

exports.Event = Event;