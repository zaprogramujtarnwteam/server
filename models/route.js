const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const crucialPoints = new Schema({
    name: String,
    description: String,
    longitude: Number,
    latitude: Number,
    imagePath: String
});

const routeSchema = new Schema({
    name: String,
    description: String,
    route: [{
        longitude: Number,
        latitude: Number
    }],
    routeType: String,
    crucialPoints: [crucialPoints],
    rating: Array,
    avgRate: Number
});

const Route = mongoose.model("Route", routeSchema);

exports.Route = Route;