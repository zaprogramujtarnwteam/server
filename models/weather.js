const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const weatherSchema = new Schema({
    city: Object,
    list: Array,
    airPollution: Array,
    airPollutionIndex: Object
});

const Weather = mongoose.model('Weather', weatherSchema);

exports.Weather = Weather;