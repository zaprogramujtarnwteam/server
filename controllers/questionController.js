const { Question } = require("../models/question");

exports.postAddQuestion = async (req, res, next) => {
    try {
        const question = await Question.create({ ...req.body });
        res.status(200).json({ confirm: true, question });
    } catch (error) {
        console.log(error);
        res.status(409).json({ confirm: false });
    }
}

exports.getRandomQuestions = async (req, res, next) => {
    Question.findRandom({}, {}, { limit: 5 }, (err, result) => {
        res.status(200).json({ confirm: true, questions: result });
        console.log(result);
    });
}
