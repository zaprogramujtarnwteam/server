const { Event } = require("../models/event");

exports.postAddEvent = async (req, res, next) => {
    try {
        const event = await Event.create({ ...req.body });
        res.status(200).json({ confirm: true, event });
    } catch (error) {
        console.log(error);
        res.status(409).json({ confirm: false });
    }
}

exports.getEvent = async (req, res, next) => {
    const { eventId } = req.params;

    try {
        const event = await Event.findById(eventId);
        res.status(200).json({ confirm: true, event });

    } catch (err) {
        console.log(err);
        res.status(409).json({ confirm: false });
    }
}

exports.getAllEvents = async (req, res, next) => {
    const events = await Event.find({});

    res.status(200).json({ confirm: true, events });
}
