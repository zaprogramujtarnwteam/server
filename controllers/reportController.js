const { Report } = require("../models/report");

exports.postAddReport = async (req, res, next) => {
    try {
        const report = await Report.create({...req.body, date: new Date()});
        res.status(200).json({ confirm: true, report });
    } catch (error) {
        console.log(error);
        res.status(409).json({ confirm: false });
    }
}

