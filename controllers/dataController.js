const { BusStop } = require('../models/busStop');
const { BikeStation } = require('../models/bikeStation');
const { Place } = require('../models/place');
const { Weather } = require('../models/weather');
const mongoose = require('mongoose');

//Bus stops

//add new bus stop
exports.postAddBusStop = async (req, res, next) => {
    try {
        const busStop = await BusStop.create({ ...req.body });
        res.status(200).json({ confirm: true, busStop });
    } catch (err) {
        console.log(err)
        res.status(409).json({ confirm: false });
    }
}

//get all bus stops data
exports.getBusStops = async (req, res, next) => {
    try {
        const busStops = await BusStop.find();
        res.status(200).json({ confirm: true, busStops });
    }
    catch (err) {
        console.log(err);
        res.status(409).json({ confirm: false });
    }
}

//Bike stations

//get all bike stations data
exports.getBikeStations = async (req, res, next) => {
    try {
        const bikeStations = await BikeStation.find();
        res.status(200).json({ confirm: true, bikeStations });
    }
    catch (err) {
        console.log(err);
        res.status(409).json({ confirm: false });
    }
}

//Places

exports.postAddPlace = async (req, res, next) => {
    try {
        const place = await Place.create({ ...req.body });
        place.qrId = place._id;
        place.save();
        res.status(200).json({ confirm: true, place });
    } catch (error) {
        console.log(error);
        res.status(409).json({ confirm: false });
    }
}

//get all available data
exports.getAllData = async (req, res, next) => {
    console.log("działam");
    try {
        const connection = mongoose.connection;
        const data = {};
        const requests = [];
        const collections = Object.keys(connection.models).sort().filter(v => v !== "Report" && v !== "Question");
        collections.forEach(async (collection) => {
            try {
                requests.push(connection.models[collection].find());
            } catch (error) {
                data[collection] = [];
                console.log(collection, error);
            }
        });
        const resps = await Promise.all(requests);
        resps.forEach((resp, i) => {
            data[collections[i]] = resp;
        })
        res.status(200).json({ confirm: true, data });

    } catch (error) {
        console.log(error);
        res.status(409).json({ confirm: false });
    }
}

//Sending weather

exports.getWeather = async (req, res, next) => {
    try {
        const weather = await Weather.findById("5cb332e11844f51849358d85");
        res.status(200).json({ confirm: true, weather });
    } catch (error) {
        console.log(error);
        res.status(409).json({ confirm: false });
    }
}

exports.getPlaceById = async (req, res, next) => {
    const { placeId } = req.params;

    try {
        const place = await Place.findOne({ qrId: placeId });
        if (!place) return res.status(404).json({ confirm: false });

        res.status(200).json({ confirm: true, place });
    } catch (err) {
        console.log(err);
        res.status(404).json({ confirm: false });
    }
}