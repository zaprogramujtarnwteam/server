const randomString = require('randomstring');
const sharp = require('sharp');
const sizeOf = require('image-size');
const AWS = require('aws-sdk');
require('dotenv').config();

const BUCKET_NAME = process.env.AMAZON_BUCKET_NAME;
const IAM_USER_KEY = process.env.AMAZON_ID;
const IAM_USER_SECRET = process.env.AMAZON_SECRET_KEY;

exports.postAddImage = async (req, res, next) => {
    // random generate filename and add original file extension
    const fileName = `${randomString.generate()}.${req.file.originalname.split('.')[1]}`;

    try {
        if (sizeOf(req.file.buffer).width > 720) { // if width is bigger then 720 - resize image
            try {
                sharp(req.file.buffer)
                    .resize(720)
                    .toBuffer()
                    .then(data => {
                        saveImage(data, fileName, res); 
                    });
            } catch (err) {
                console.log(err);
                res.status(404).json({ confirm: false, message: "Wgrywanie zdjęcia nie powiodło się" });
            }
        } else { // if not - save image with original size
            try {
                const file = req.file.buffer;

                saveImage(file, fileName, res);
            } catch (err) {
                console.log(err);
                res.status(404).json({ confirm: false, message: "Wgrywanie zdjęcia nie powiodło się" });
            }
        }
    } catch (err) {
        console.log(err);
        res.status(404).json({ confirm: false, message: "Wgrywanie zdjęcia nie powiodło się" });
    }
}

exports.getImage = async (req, res, next) => {
    const { fileName } = req.params;

    getImage(fileName, res);
}

const saveImage = (file, fileName, res) => {
    const s3bucket = new AWS.S3({
        accessKeyId: IAM_USER_KEY,
        secretAccessKey: IAM_USER_SECRET,
        Bucket: BUCKET_NAME,
    });
    s3bucket.createBucket(() => {
        const params = {
            Bucket: BUCKET_NAME,
            Key: fileName,
            Body: file,
        };
        s3bucket.upload(params, (err, data) => {
            if (err) return res.status(404).json({ confirm: false, message: "Wgrywanie zdjęcia nie powiodło się", error: err });

            res.status(200).json({ confirm: true, message: "Pomyślnie wgrano zdjęcie", fileName: fileName });
        });
    });
}

const getImage = (fileName, res) => {
    const s3bucket = new AWS.S3({
        accessKeyId: IAM_USER_KEY,
        secretAccessKey: IAM_USER_SECRET,
        Bucket: BUCKET_NAME,
    });

    s3bucket.createBucket(() => {
        const options = {
            Bucket: `${BUCKET_NAME}`,
            Key: fileName
        }
        const fileStream = s3bucket.getObject(options).createReadStream().on('error', err => {
            if (err) return res.status(404).json({ confirm: false, message: "Brak pliku o takiej nazwie" });
        });
        fileStream.pipe(res);
    });
}