const { Route } = require("../models/route");

exports.postAddRoute = async (req, res, next) => {
    try {
        const route = await Route.create({ ...req.body });
        res.status(200).json({ confirm: true, route });
    } catch (error) {
        console.log(error);
        res.status(409).json({ confirm: false });
    }
}


exports.postAddCrucialPoints = async (req, res, next) => {
    const { routeId } = req.params;

    try {
        const route = await Route.findById(routeId);

        route.crucialPoints.push({ ...req.body });
        route.save();

        res.status(200).json({ confirm: true });
    } catch (error) {
        console.log(error);
        res.status(409).json({ confirm: false })
    }
}

exports.patchRateRoute = async (req, res, next) => {
    const { routeId } = req.params;

    console.log(routeId)

    try {
        const route = await Route.findById(routeId);

        route.rating.push({ ...req.body });

        const avgRate = route.rating.reduce((prevValue, nextValue) => prevValue + nextValue, 0);
        route.avgRate = avgRate / route.rating.length;

        route.save();

        res.status(200).json({ confirm: true });
    } catch (error) {
        console.log(error);
        res.status(409).json({ confirm: false })
    }
}

exports.getRoutes = async (req, res, next) => {
    const routes = await Route.find({});

    res.status(200).json({ confirm: true, routes });
}