const { BikeStation } = require('./models/bikeStation');
const { Weather } = require('./models/weather');

const axios = require("axios");
const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const HtmlTableToJson = require('html-table-to-json');

const fetchWeather = async () => {
    try {
        const weather = await axios.get("https://api.openweathermap.org/data/2.5/forecast/daily?id=7532542&APPID=88a3a9dad6d7d30bf94e8132a89460b0&cnt=7");
        const airPollutionData = await axios.get("http://api.gios.gov.pl/pjp-api/rest/station/sensors/10120");
        const airPollutionIndex = await axios.get("http://api.gios.gov.pl/pjp-api/rest/aqindex/getIndex/10120");

        const weatherObject = await Weather.findById("5cb332e11844f51849358d85");
        weatherObject.list = weather.data.list;
        weatherObject.airPollution = airPollutionData.data;
        weatherObject.airPollutionIndex = airPollutionIndex.data.stIndexLevel;
        weatherObject.save();
        console.log("weather updated");
    } catch (err) {
        console.log(err)
    }
}

exports.updateStationData = async (req, res, next) => {
    setInterval(async () => {
        try {
            const stationsData = await axios.get("https://rower.tarnow.pl/mapa-stacji/");

            const dom = new JSDOM(stationsData.data);
            const jsonResult = new HtmlTableToJson(dom.window.document.querySelector(".station_list").innerHTML);
            const data = jsonResult.results[0];

            data.forEach(station => {
                BikeStation.findOneAndUpdate({ name: station.Lokalizacja }, {
                    availableBikes: station["Dostępne rowery"],
                    availableStands: station["Wolne stojaki"],
                    standsAmount: station["Ilość stojaków"]
                })
            });

            console.log("update bike stations");
        } catch (err) {
            console.log(err);
        }
    }, 1000 * 60);
}

exports.updateWeather = async (req, res, next) => {
    fetchWeather();
    setInterval(fetchWeather, 1000 * 60 * 5);
}