document.addEventListener('DOMContentLoaded', () => {
    imagename = "";
    const btnplace = document.querySelector(".send-place");
    const image = document.querySelector(".foto");

    btnplace.addEventListener("click", () => {
        const data = {
            longitude: parseFloat(document.querySelector("#place .lon").value),
            latitude: parseFloat(document.querySelector("#place .lat").value),
            category: document.querySelector("#place .cat").value,
            subcategory: document.querySelector("#place .subcat").value,
            name: document.querySelector("#place .name").value,
            description: document.querySelector("#place .desc").value,
            workingHours: [document.querySelector("#place .open").value, document.querySelector("#place .close").value],
            address: document.querySelector("#place .addr").value,
            website: document.querySelector("#place .website").value,
            email: document.querySelector("#place .email").value,
            phone: document.querySelector("#place .tel").value,
            priceLevel: document.querySelector("#place .price").value,
            imagePath: imagename
        }
        console.log(data);

        axios.post('http://192.168.0.31:5000/api/data/add/place', data);
    })
    image.addEventListener("change", async (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append('image', e.target.files[0], e.target.files[0].name);

        const res = await axios({
            url: 'http://192.168.0.31:5000/api/image/add',
            method: 'POST',
            headers: { 'Content-Type': 'multipart/form-data' },
            data: formData
        });

        if (!res.data.error) {
            
            document.querySelector(".image-name").innerHTML = res.data.fileName;
            imagename= res.data.fileName;
            
        }
    })
});