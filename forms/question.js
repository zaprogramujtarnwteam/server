document.addEventListener('DOMContentLoaded', () => {
    imagename = "";
    const btnsend = document.querySelector(".send");
    const image = document.querySelector(".foto");

    btnsend.addEventListener("click", () => {
        const data = {
            question: document.querySelector(".q").value,
            answers: [{ text: document.querySelector(".a1").value, correct: true }, { text: document.querySelector(".a2").value }, { text: document.querySelector(".a3").value }],
            imagePath: imagename
        }
        console.log(data);
        axios.post('http://192.168.0.31:5000/api/question/add', data);
    });
    image.addEventListener("change", async (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append('image', e.target.files[0], e.target.files[0].name);

        const res = await axios({
            url: 'http://192.168.0.31:5000/api/image/add',
            method: 'POST',
            headers: { 'Content-Type': 'multipart/form-data' },
            data: formData
        });

        if (!res.data.error) {

            document.querySelector(".image-name").innerHTML = res.data.fileName;
            imagename = res.data.fileName;

        }
    })
});